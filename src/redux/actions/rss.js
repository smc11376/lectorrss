import * as types from '../types/rss'

export function updateRSSList(value) {
  return {
    type: types.RSS_UPDATE_LIST,
    value
  }
}

export function setFetching(value) {
  return {
    type: types.RSS_UPDATE_FETCHING,
    value
  }
}