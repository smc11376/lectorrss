import * as types from '../types/rss'

const initialState = {
  list: [],
  isFetching: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {

    case types.RSS_UPDATE_LIST:
      return {
        ...state,
        list: action.value,
      };
    
    case types.RSS_UPDATE_FETCHING:
      return {
        ...state,
        isFetching: action.value,
      };
    default:
      return state;
  }
}
