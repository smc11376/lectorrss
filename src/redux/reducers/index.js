import { combineReducers } from 'redux';

import rss from './rss'

export default combineReducers({
  rss,
});