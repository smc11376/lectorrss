import { AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux'
import _ from 'lodash'

import * as Api from '../../api'
import * as Constants from '../../api/Constants'
import * as rssActions from '../actions/rss'

// Comprobación de datos guardados en memoria interna del teléfono
export function checkSession() {
  return (dispatch, getState) => {
    // Recuperamos el valor con la clave
    AsyncStorage.getItem('rssList', (err, rssList) => {
      if(err) console.log('AsyncStorage error: ', err)
      // Si hay datos los convertimos en JSON y los guardamos en Redux
      if(rssList) {
        const list = JSON.parse(rssList)
        Constants.LOG_ENABLED && console.log('AsyncStorage rssList: ', list)
        dispatch(rssActions.updateRSSList(list))
      }
      // Pasados dos segundos navegamos a la pantalla Home
      setTimeout(() => { Actions.reset('Home') }, 2000)
    })
  }
}

// Petición a la API del listado de noticias de RSS
export function getRSSList(params) {
  return (dispatch, getState) => {

    // Indicamos que empezamos a realizar una petición
    dispatch(rssActions.setFetching(true))
    Constants.LOG_ENABLED && console.log("getRSSList params: ", params)

    Api.getRSSList(params).then((response) => {
      Constants.LOG_ENABLED && console.log("getRSSList response: ", response)
      // Al obtener respuesta indicamos que hemos terminado de realizar la petición
      dispatch(rssActions.setFetching(false))

      // Si el resultado tiene status OK guardamos resultado
      if(response.status === Constants.API_RESPONSE_OK) {
        // Ordenamos por fecha 'puDate' de forma descendente
        const list = _.orderBy(_.get(response, 'items', []), ['pubDate'], ['desc'])
        // Guardamos el listado en memoria interna del dispositivo
        AsyncStorage.setItem('rssList', JSON.stringify(list))
        // Guardamos en Redux el listado
        dispatch(rssActions.updateRSSList(list))
      }
    }).catch((error) => {
      Constants.LOG_ENABLED && console.log("getRSSList error: ", error)
      // Si la petición se resuelve con error indicamos que hemos terminado de realizar la petición
      dispatch(rssActions.setFetching(false))
    })
  }
}