import { StyleSheet } from 'react-native'
import { initialScale } from '../../../commons/Utils';
import { Colors } from '../../../commons';

export default StyleSheet.create({
  container: { width: 50*initialScale, height: 50*initialScale, alignItems: 'center', justifyContent: 'center' },
  image: { width: 30*initialScale, height: 30*initialScale, transform: [{ rotate: '270deg'}], tintColor: Colors.main_color }
})
