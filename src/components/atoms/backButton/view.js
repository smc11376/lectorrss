import { TouchableOpacity, Image } from 'react-native'
import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'

import styles from './styles.js'

export default class BackButton extends Component {
  render() {
    return(
      <TouchableOpacity style={styles.container} onPress={ () => Actions.pop() } >
        <Image style={styles.image} source={require('../../../assets/images/arrow.png')} resizeMode={'cover'} />
      </TouchableOpacity>
		)
	}
}