import { TouchableOpacity, Text } from 'react-native'
import React, { Component } from 'react'

import styles from './styles.js'

export default class Button extends Component {
  render() {
    const { label, onPress } = this.props
    return(
      <TouchableOpacity style={styles.container} onPress={ onPress } >
        <Text style={styles.label}>{label}</Text>
      </TouchableOpacity>
		)
	}
}