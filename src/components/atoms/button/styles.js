import { StyleSheet } from 'react-native'
import { initialScale } from '../../../commons/Utils';
import { Colors } from '../../../commons';

export default StyleSheet.create({
  container: { backgroundColor: Colors.main_color, padding: 20*initialScale, alignItems: 'center', justifyContent: 'center', borderRadius: 10 },
  label: { color: Colors.white }
})
