import { StyleSheet } from 'react-native'
import { screenWidth } from '../../../commons/Utils';

export default StyleSheet.create({
  container: {  },
  image: { width: screenWidth, height: screenWidth }
})
