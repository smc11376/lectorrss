import { View, Image } from 'react-native'
import React, { Component } from 'react'
import PropTypes from 'prop-types';

import styles from './styles.js'

export default class HeaderImage extends Component {
  render() {
    const { img } = this.props

    return(
      <View style={styles.container}>
        <Image style={styles.image} source={img} resizeMode={'cover'}/>
      </View>
		)
	}
}

HeaderImage.propTypes = {
  img: PropTypes.object,
};