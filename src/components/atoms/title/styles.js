import { StyleSheet } from 'react-native'
import { Colors } from '../../../commons/'

export default StyleSheet.create({
  label: { flex: 1, fontSize: 18, color: Colors.main_color },
})
