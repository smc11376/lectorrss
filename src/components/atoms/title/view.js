import { Text } from 'react-native'
import React, { Component } from 'react'
import PropTypes from 'prop-types';

import styles from './styles.js'

export default class Title extends Component {
  formatSize(size) {
    switch (size) {
      case 'small':
        return 14
      case 'medium':
        return 18
      case 'large':
        return 22
    
      default:
        return 14
    }
  }

  render() {
    const { label, size, numberOfLines } = this.props
    const formattedSize = this.formatSize(size)
    return(
      <Text style={[styles.label, { fontSize: formattedSize }]} numberOfLines={numberOfLines}>{label}</Text>
		)
	}
}

Title.propTypes = {
  label: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  numberOfLines: PropTypes.number,
};

Title.defaultProps = {
  label: '',
  size: 'small'
};