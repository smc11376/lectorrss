import { StyleSheet } from 'react-native'
import { Colors, Utils } from '../../../commons/'

const initialScale = Utils.initialScale

export default StyleSheet.create({
  container: { flexDirection: 'row' },
  mainText: { fontSize: 14, color: Colors.text_color }
})
