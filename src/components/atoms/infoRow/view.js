import { View, Text } from 'react-native'
import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './styles.js'

export default class InfoRow extends Component {
  render() {
    const { mainText, numberOfLines } = this.props

    return(
      <View style={styles.container}>
        <Text style={styles.mainText} numberOfLines={numberOfLines}>{mainText}</Text>
      </View>
		)
	}
}

InfoRow.propTypes = {
  mainText: PropTypes.string,
  numberOfLines: PropTypes.number,
};

InfoRow.defaultProps = {
  mainText: ''
};