import Title from './title/view'
import InfoRow from './infoRow/view'
import HeaderImage from './headerImage/view'
import BackButton from './backButton/view'
import Button from './button/view'

export {
  Title,
  InfoRow,
  HeaderImage,
  BackButton,
  Button
}