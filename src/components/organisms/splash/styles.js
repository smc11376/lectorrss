import { StyleSheet } from 'react-native'
import { Colors, Utils } from '../../../commons/'

export default StyleSheet.create({
  container: { flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' },
  text: { fontSize: 28*Utils.initialScale, color: Colors.main_color }
})
