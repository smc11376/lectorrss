import { View, Text } from 'react-native'
import React, { Component } from 'react'

import { configureAxios } from '../../api/'
import styles from './styles.js'

export default class Splash extends Component {
  componentDidMount() {
    // Al iniciar la aplicación configuramos nuestro gestor de peticiones a la API
    configureAxios(null)
    // Y comprobamos los datos en memoria del dispositivo
    this.props.checkSession()
  }

  render() {

    return(
      <View style={styles.container}>
        <Text style={styles.text}>Lector RSS</Text>
      </View>
		)
	}
}