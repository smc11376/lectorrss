import { connect } from 'react-redux'
import * as rssOperations from '../../../redux/operations/rss'

import view from './view'

function mapDispatchToProps (dispatch, props) {
  return {
    checkSession: () => {
      dispatch(rssOperations.checkSession())
    }
  }
}

export default connect (null, mapDispatchToProps)(view)