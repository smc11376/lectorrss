import { View } from 'react-native'
import React, { Component } from 'react'
import _ from 'lodash'

import styles from './styles.js'
import { RSSList, Filter } from '../../molecules/'
import * as Constants from '../../../api/Constants'

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: ''
    }
  }
  
  componentDidMount() {
    // Al entrar en la Home hacemos la petición a la API del listado de noticias
    this.props.getRSSList({ rss_url: 'https://ep00.epimg.net/rss/elpais/portada.xml', api_key: Constants.API_KEY  })
  }

  render() {
    const { isFetching, rssList } = this.props
    const { searchText } = this.state

    // Filtramos el listado con el texto introducido en el buscador
    const list = _.filter(rssList, item => item.title.toLowerCase().indexOf(searchText.toLowerCase()) !== -1)

    return(
      <View style={styles.container}>
        <Filter value={searchText} onChangeText={ (value) => this.setState({ searchText: value })} />
        <RSSList
          list={list}
          isFetching={isFetching}
          refreshList={ () => this.props.getRSSList() }
        />
      </View>
		)
	}
}