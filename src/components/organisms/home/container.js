import { connect } from 'react-redux'
import * as rssOperations from '../../../redux/operations/rss'

import view from './view'

function mapStateToProps(state) {
  return {
    rssList: state.rss.list,
    isFetching: state.rss.isFetching,
  }
}

function mapDispatchToProps (dispatch, props) {
  return {
    getRSSList: (params) => {
      dispatch(rssOperations.getRSSList(params))
    }
  }
}

export default connect (mapStateToProps,mapDispatchToProps)(view)