import Splash from './splash/container'
import Home from './home/container'
import Detail from './detail/container'

export {
  Splash,
  Home,
  Detail
}