import { connect } from 'react-redux'
import * as rssOperations from '../../../redux/operations/rss'

import view from './view'

function mapStateToProps(state) {
  return {
    
  }
}

function mapDispatchToProps (dispatch, props) {
  return {
    
  }
}

export default connect (mapStateToProps,mapDispatchToProps)(view)