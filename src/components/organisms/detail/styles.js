import { StyleSheet } from 'react-native'
import { Colors, Utils } from '../../../commons/'

const initialScale = Utils.initialScale

export default StyleSheet.create({
  container: { flex: 1, backgroundColor: Colors.white },
  backContainer: { position: 'absolute', top: 20, left: 0 },
  titleContainer: { padding: 20*initialScale },
  sectionContainer: { paddingHorizontal: 20*initialScale, paddingVertical: 10*initialScale },
  ratingContainer: { flexDirection: 'row', alignItems: 'center' },
  label: { fontSize: 18, color: Colors.main_color },
  description: { fontSize: 14, color: Colors.text_color },
  buttonContainer: { flex: 1, padding: 20*initialScale, justifyContent: 'flex-end' }
})
