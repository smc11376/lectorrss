import { View, ScrollView, Linking } from 'react-native'
import React, { Component } from 'react'
import _ from 'lodash'

import styles from './styles.js'
import { BackButton, HeaderImage, Title, InfoRow, Button } from '../../atoms/'

export default class Detail extends Component {
  onLinkPress(link) {
    Linking.openURL(link).catch(err => console.error('Error al abrir url: ', err))
  }

  render() {
    const { data } = this.props

    const mainPhoto = { uri: _.get(data, 'enclosure.link', null) }
    const title = _.get(data, 'title', '')
    const description = _.get(data, 'description', '')
    const link = _.get(data, 'link', '')

    return(
      <View style={styles.container}>
        <ScrollView bounces={false} >
          <HeaderImage img={mainPhoto} />
          <View style={styles.backContainer}>
            <BackButton />
          </View>

          <View style={styles.sectionContainer}>
            <Title label={title} size={'large'} />
          </View>

          <View style={styles.sectionContainer}>
            <InfoRow mainText={description} />
          </View>

          <View style={styles.buttonContainer}>
            <Button label={'Ver en el navegador'} onPress={ () => this.onLinkPress(link) } />
          </View>
        </ScrollView>
      </View>
		)
	}
}