// BASIC COMPONENTS
import React, { Component } from 'react'

// REDUX COMPONENTS
import { createStore, applyMiddleware, compose, } from 'redux'
import { Provider, connect } from 'react-redux'
import thunk from 'redux-thunk'

// ROUTES
import Routes from './Routes'

// REDUCERS
import reducers from '../../redux/reducers/'

// INITIALIZE REDUX STORE
const store = compose(
  applyMiddleware(thunk)
)(createStore)(reducers);

export default class App extends Component {
  render() {
    console.disableYellowBox = true;

    return (
      <Provider store={store} >
        <Routes />
      </Provider>
    )
  }
}
