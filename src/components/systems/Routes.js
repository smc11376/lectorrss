// BASIC COMPONENTS
import React, { Component } from 'react'

// NAVIGATION COMPONENTS (ROUTER FLUX)
import { Scene, Router, Reducer } from 'react-native-router-flux'

// SCENES
import { Splash, Home, Detail } from '../organisms/'

const reducerCreate = params => {
  const defaultReducer = new Reducer(params);
  return (state, action) => {
    return defaultReducer(state, action);
  }
}

export default class Routes extends Component {

  render() {
    return (
      <Router createReducer={reducerCreate}>
        <Scene key="root" backButtonTintColor={'green'} >
          <Scene key='Splash' component={Splash} hideNavBar initial={true} type = 'reset'/>
          <Scene key='Home' component={Home} title={'Home'} type = 'reset'/>
          <Scene key='Detail' component={Detail} hideNavBar/>
        </Scene>
      </Router>
    )
  }
}
