import { FlatList, TouchableOpacity, RefreshControl } from 'react-native'
import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import _ from 'lodash'

import styles from './styles.js'
import { Card } from '../'

export default class RSSList extends Component {
  renderCard(data) {
    const item = _.get(data, 'item', {})
    const index = _.get(data, 'index', 0)
    return(
      <TouchableOpacity key={'card'+index} style={styles.card} onPress={ () => Actions.Detail({ data: item }) }>
        <Card data={item} />
      </TouchableOpacity>
    )
  }

  render() {
    const { list, isFetching } = this.props

    return(
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={isFetching}
            onRefresh={this.props.refreshList}
          />
        }
        style={styles.container}
        keyExtractor={ (item, index) => index.toString() }
        data={list}
        renderItem={ this.renderCard.bind(this) } />
		)
	}
}