import { StyleSheet } from 'react-native'
import { initialScale } from '../../../commons/Utils';
import { Colors } from '../../../commons';

export default StyleSheet.create({
  container: { flex: 1, backgroundColor: Colors.white },
  card: { padding: 0*initialScale }
})
