import { StyleSheet } from 'react-native'
import { Colors, Utils } from '../../../commons/'

const initialScale = Utils.initialScale

export default StyleSheet.create({
  container: { backgroundColor: Colors.white, flexDirection: 'row', borderColor: Colors.black, borderWidth: 0.5, shadowOffset: { width: 0, height: 0 }, shadowOpacity: 0.7 },
  photoContainer: {  },
  mainPhoto: { width: 100*initialScale, height: 100*initialScale },
  infoContainer: { flex: 1, padding: 10*initialScale },
})
