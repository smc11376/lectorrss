import { View, Image } from 'react-native'
import React, { Component } from 'react'
import _ from 'lodash'

import styles from './styles.js'
import { Title, InfoRow } from '../../atoms/'

export default class Card extends Component {
  render() {
    const { data } = this.props

    const mainPhoto = { uri: _.get(data, 'enclosure.link', null) }
    const title = _.get(data, 'title', '')
    const description = _.get(data, 'description', '')

    return(
      <View style={styles.container}>
        <View style={styles.photoContainer}>
          <Image style={styles.mainPhoto} source={mainPhoto} resizeMode={'cover'}/>
        </View>
        <View style={styles.infoContainer} >
          <Title label={title} size={'large'} numberOfLines={2} />
          <InfoRow mainText={description} numberOfLines={2} />
        </View>
      </View>
		)
	}
}