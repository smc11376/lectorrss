import { View, TextInput } from 'react-native'
import React, { Component } from 'react'

import styles from './styles.js'

export default class Filter extends Component {
  render() {
    const { value, onChangeText } = this.props

    return(
      <View style={styles.container}>
        <TextInput placeholder={'Buscar...'} style={styles.input} value={value} onChangeText= { onChangeText } />
      </View>
		)
	}
}