import { StyleSheet } from 'react-native'
import { Colors } from '../../../commons/'

export default StyleSheet.create({
  container: { height: 40, paddingHorizontal: 20 },
  input: { flex: 1, fontSize: 18, color: Colors.main_color },
})
