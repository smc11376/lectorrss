import { Dimensions } from 'react-native'

export const screenHeight = Dimensions.get('window').height
export const screenWidth = Dimensions.get('window').width

// RESPONSIVE
const IPHONE6_WIDTH = 375;
export const initialScale = screenWidth / IPHONE6_WIDTH > 1 ? 1 : screenWidth / IPHONE6_WIDTH