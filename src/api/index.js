import { configureAxiosDefaults, fetch } from './Config'
import qs from 'qs'

export function configureAxios(token) {
  configureAxiosDefaults(token)
}

export function getRSSList(params) {
  const url = '/v1/api.json?' + qs.stringify( params, { skipNulls: true } )
  return fetch(url)
}