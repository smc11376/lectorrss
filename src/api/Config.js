import axios from 'axios'
import * as Constants from './Constants'

// Configuración del gestor de peticiones a la API
export function configureAxiosDefaults(token) {
  Constants.LOG_ENABLED && console.log("configureAxiosDefaults token: ", token)
  axios.defaults.baseURL = Constants.BASE_URL
  if(token) axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
  axios.defaults.headers.post['Content-type'] = 'application/json'
}

export function fetch (url) {
  return axios.get(url).then((response) => {
    return response.data ? response.data : null
  }).catch((error) => {
    if (error.response) {
      throw {code: error.response.status, msg: error.response.data, error: error}
    } else {
      throw {code: 500, msg: error.message, error: error}
    }
  });
}