import { AppRegistry } from 'react-native'
import App from './src/components/systems/App'

AppRegistry.registerComponent('LectorRSS', () => App);