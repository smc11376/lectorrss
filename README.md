API:

Se usa la API https://api.rss2json.com capaz de convertir los datos xml de los RSS en objetos JSON para así poder trabajr comodamente en Javascript


Estructura:

Se usa diseño atómico que consiste en dividir los componentes en:
  · Átomos: componentes sencillos que se repetirán a lo largo de toda la aplicación (título, botón, imagen tipo)
  · Moléculas: componentes sencillos sin lógica que pueden englobar varios átomos (celda de un listado)
  · Organismos: componentes conectados a Redux independientes unos de otros (filtros avanzados, listado con paginación)
  · Páginas: componentes específicos de las páginas por las que se va a navegar (Splash, pantalla de login, home)
  · Sistemas: componentes más grandes que engloban la aplicación en general (App.js y el componente con la navegación)

En este caso se han suprimido las páginas ya que es una aplicación sencilla y no ha sido necesario dividir dividir tanto las categorías.


Librerías:

Se han usado las mínimas necesarias:
  · axios: gestión de datos en Javascript
  · redux: gestión de datos globales 


Redux:

Aunque no era necesaria para esta aplicación me ha parecido interesante explicar mi forma de organizar los archivos de Redux:
  · Types: identificador único para para variable
  · Reducer: gestión del propio reducer de Redux
  · Actions: funciones para modificar directamente variables de Redux
  · Operations: funciones con mayor lógica, así como las llamadas a la API